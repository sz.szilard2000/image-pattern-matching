#include "PatternSearchKMP.h"
#include <time.h>
#include <iomanip>

#define q 2147483647            /// nagy primszam
#define nrCharacters 256        /// karakterek szama

using namespace std;

FILE* pic1 = fopen("picture/2.ppm", "r");
FILE* pic2 = fopen("pattern/2.ppm", "r");
FILE* res = fopen("results/22.ppm", "w");

struct RGB{
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

int hany;

class PictureSearch
{
    RGB **text;                         /// kep1
    RGB **text2;                        /// eredmeny kep
    int textRow, textCol;               /// kep1 meretei
    RGB **pattern;                      /// minta
    int patternRow, patternCol;         /// minta meretei
    int **textRK;                       /// RK matrix kep1-re
    int *patternRK;                     /// RK tomb mintara
    long nrPow = 1;                     /// a hatvany kiszamitasa

public:
    PictureSearch() {}
    void Read();
    bool Equal() const;
    void TextHash();
    void PatternHash();
    bool Check(int row, int col) const;
    void Matching();
    void Draw(int row, int col);
    void Write() const;
};

int convert(RGB x)                                  /// atalakitja int szamma az RGB kodot
{
    return 5 * x.r + 13 * x.g + 17 * x.b;
}

void PictureSearch :: Read()                        /// beolvasas, memoria lefoglalas
{
    int r, g, b;
    int x;
    char s[25];

    fscanf(pic1, "%s", s);
    fscanf(pic1, "%d%d%d", &textRow, &textCol, &x);
    text = new RGB* [textRow];
    text2 = new RGB* [textRow];
    for (int i = 0; i < textRow; i ++)
    {
        text[i] = new RGB [textCol];
        text2[i] = new RGB [textCol];
    }
    for (int i = 0; i < textRow; i ++)
    {
        for (int j = 0; j < textCol; j ++)
        {
            fscanf(pic1, "%d%d%d", &r, &g, &b);
            text[i][j].r = r;
            text[i][j].g = g;
            text[i][j].b = b;
            text2[i][j].r = r;
            text2[i][j].g = g;
            text2[i][j].b = b;
        }
    }

    fscanf(pic2, "%s", s);
    fscanf(pic2, "%d%d%d", &patternRow, &patternCol, &x);
    pattern = new RGB* [patternRow];
    for (int i = 0; i < patternRow; i ++)
    {
        pattern[i] = new RGB [patternCol];
    }

    for (int i = 0; i < patternRow; i ++)
    {
        for (int j = 0; j < patternCol; j ++)
        {
            fscanf(pic2, "%d%d%d", &r, &g, &b);
            pattern[i][j].r = r;
            pattern[i][j].g = g;
            pattern[i][j].b = b;
        }
    }

    for (int i = 0; i < patternRow - 1; i ++)   /// hatvany kiszamolasa
    {
        nrPow = (nrPow * nrCharacters) % q;
    }
}

void PictureSearch :: TextHash()                    /// az elso kep atalakitasa az RK-s hasito fuggvennyel
{
    textRK = new int* [textRow - patternRow + 1];
    for (int i = 0; i < textRow - patternRow + 1; i ++)
    {
        textRK[i] = new int [textCol];
    }

    for (int j = 0; j < textCol; j ++)
    {
        textRK[0][j] = 0;
        for (int i = 0; i < patternRow; i ++)
        {
            textRK[0][j] =  (textRK[0][j] * nrCharacters + convert(text[i][j])) % q;
        }
    }

    for (int i = patternRow; i < textRow; i ++)
    {
        for (int j = 0; j < textCol; j ++)
        {
            int prev = textRK[i - patternRow][j];
            int lead = convert(text[i - patternRow][j]);
            int nw = convert(text[i][j]);
            textRK[i - patternRow + 1][j] = (nrCharacters * (prev - lead * nrPow) + nw) % q;
        }
    }
}

void PictureSearch :: PatternHash()                 /// a minta kep atalakitasa az RK-s hasito fuggvennyel
{
    patternRK = new int [patternCol];
    for (int j = 0; j < patternCol; j ++)
    {
        patternRK[j] = 0;
        for (int i = 0; i < patternRow; i ++)
        {
            patternRK[j] = (patternRK[j] * nrCharacters) + convert(pattern[i][j]) % q;
        }
    }
}

bool PictureSearch :: Check(int row, int col) const /// a talalt koordinatak leellenorzese
{
    for (int i = 0; i < patternRow; i ++)                   /// vegigmegyunk a talalt matrixon, ha elteres van nullat terit vissza
    {
        for (int j = 0; j < patternCol; j ++)
        {
            if(pattern[i][j].r != text[row + i][col + j].r || pattern[i][j].g != text[row + i][col + j].g || pattern[i][j].b != text[row + i][col + j].b)
            {
                return 0;
            }
        }
    }
    return 1;
}

void PictureSearch :: Matching()                    /// a kereses
{
    int row = 0;
    hany = 1;
    do
    {
        KMP<int> O(textRK[row], patternRK, textCol, patternCol);                                      /// objektum letrehozasa
        for (int col : O.KMPSearchAll())                                                                /// a KMP talalatainak leellenorzese
        {
            if (Check(row, col))                                                                        /// egyezes eseten kiiras es bekeretezes a kepen
            {
                printf("Megvan: %d. %d %d\n", hany ++, row, col);
                Draw(row, col);
            }
        }
        row ++;                                                                                         /// noveljuk a sort
    } while(row < textRow - patternRow + 1);
}

void PictureSearch :: Draw(int row, int col)        /// a minta bekeretezese
{
    for(int i = 0; i < patternCol + 3; i ++)   /// felso, also
    {
        if (row - 3 >= 0 && col + i < textCol)
        {
            text2[row - 3][col + i].r = 255;    text2[row - 3][col + i].g = 0;      text2[row - 3][col + i].b = 0;
            text2[row - 2][col + i].r = 255;    text2[row - 2][col + i].g = 0;      text2[row - 2][col + i].b = 0;
            text2[row - 1][col + i].r = 255;    text2[row - 1][col + i].g = 0;      text2[row - 1][col + i].b = 0;
        }
        if (row + patternRow + 2 < textRow && col + i - 3 >= 0)
        {
            text2[row + 2 + patternRow][col + i - 3].r = 255;   text2[row + 2 + patternRow][col + i - 3].g = 0;     text2[row + 2 + patternRow][col + i - 3].b = 0;
            text2[row + 1 + patternRow][col + i - 3].r = 255;   text2[row + 1 + patternRow][col + i - 3].g = 0;     text2[row + 1 + patternRow][col + i - 3].b = 0;
            text2[row + patternRow][col + i - 3].r = 255;       text2[row + patternRow][col + i - 3].g = 0;         text2[row + patternRow][col + i - 3].b = 0;
        }
    }
    for (int i = 0; i < patternRow + 3; i ++)   /// bal, jobb
    {
        if (row + i - 3 >= 0 && col - 3 >= 0)
        {
            text2[row + i - 3][col - 3].r = 255;    text2[row + i - 3][col - 3].g = 0;     text2[row + i - 3][col - 3].b = 0;
            text2[row + i - 3][col - 2].r = 255;    text2[row + i - 3][col - 2].g = 0;     text2[row + i - 3][col - 2].b = 0;
            text2[row + i - 3][col - 1].r = 255;    text2[row + i - 3][col - 1].g = 0;     text2[row + i - 3][col - 1].b = 0;
        }
        if (row + i < textRow && col + patternCol + 3 < textCol)
        {
            text2[row + i][col + 2 + patternCol].r = 255;   text2[row + i][col + 2 + patternCol].g = 0;     text2[row + i][col + 2 + patternCol].b = 0;
            text2[row + i][col + 1 + patternCol].r = 255;   text2[row + i][col + 1 + patternCol].g = 0;     text2[row + i][col + 1 + patternCol].b = 0;
            text2[row + i][col + 0 + patternCol].r = 255;   text2[row + i][col + 0 + patternCol].g = 0;     text2[row + i][col + 0 + patternCol].b = 0;
        }
    }
}

bool PictureSearch :: Equal() const                 /// ellenorzi hogy a kep megegyezik a mintaval, itt nem hasznalom mert nincs ilyen teszteset
{
    if (textRow != patternRow || textCol != patternCol)
    {
        return 0;
    }
    for (int i = 0; i < textRow; i ++)
    {
        for (int j = 0; j < textCol; j ++)
        {
            if (text[i][j].r != pattern[i][j].r || text[i][j].g != pattern[i][j].g || text[i][j].b != pattern[i][j].b)
                return 0;
        }
    }
    return 1;
}

void PictureSearch :: Write() const                 /// az eredmeny kep beirasa allomanyba
{
    fprintf(res, "P3\n");
    fprintf(res, "%d %d %d\n", textRow, textCol, 255);
    for (int i = 0; i < textRow; i ++)
    {
        for (int j = 0; j < textCol; j ++)
        {
            fprintf(res, "%d %d %d\n", text2[i][j].r, text2[i][j].g, text2[i][j].b);
        }
    }
}

int main()
{
    clock_t t,t1;
    t = clock();
    PictureSearch A;
    A.Read();
    t1 = clock() - t;
    if (!A.Equal())
    {
        //cout << fixed << setprecision(3) << (float)t1/CLOCKS_PER_SEC << endl;
        A.TextHash();
        A.PatternHash();
        A.Matching();
        t = clock() - t;
        //cout << fixed << setprecision(3) << (float)t/CLOCKS_PER_SEC << endl;
        A.Write();
        if (hany == 1)
            cout << "Nem talalhato a minta!";
    }
    else
    {
        cout << "Ugyanaz a ket kep!";
    }
    return 0;
}

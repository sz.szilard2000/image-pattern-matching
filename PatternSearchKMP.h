#ifndef PATTERNSEARCHKMP_H_INCLUDED
#define PATTERNSEARCHKMP_H_INCLUDED

#include <iostream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vector>

using namespace std;

template <class T> class KMP
{
    T *text;                                                        /// szoveg
    T *pattern;                                                     /// KMP
    int *lps;                                                       /// prefix tomb
    int textLength;                                                 /// szoveg hossz
    int patternLength;                                              /// KMP hossz

public:
    KMP(T*, T*, int, int);                                          /// konstruktor
    ~KMP();                                                         /// destruktor
    bool TextEmpty() const;                                         /// ellenorzi hogy a szoveg ures-e
    bool PatternEmpty() const;                                      /// ellenoryi hogy a KMP ures-e
    bool Equal() const;                                             /// ellenorzi hogy a ket tomb egyforma-e
    void LPSArray();                                                /// meghatarozza a prefix tomb elemeit
    int KMPSearch();                                                /// KMP KMPkereses 1 talalatra
    vector<int> KMPSearchAll();                                     /// KMP KMPkereses az osszes talalatra
};

#endif // PATTERNSEARCHKMP_H_INCLUDED

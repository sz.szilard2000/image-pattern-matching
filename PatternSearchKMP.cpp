#include "PatternSearchKMP.h"

/// Leiras: Konstruktor, lefoglalja a memoriat es inicializalja a text es pattern tomboket, valamint erteket kapnak a textLength es patternLength valtozok
/// Elofeltetelek: nincs
/// Utofeltetelek: letrejon a KMP osztaly egy objektuma
/// Parameterek:
/// T *txt - T tipusu tomb, ebben keressuk majd a mintat
/// T *pat - T tipusu tomb, ezt a mintat keressuk a txt tombben
/// int txtLength - a txt tomb hossza
/// int patLength - a minta tomb hossza
template <class T> KMP<T> :: KMP(T *txt, T *pat, int txtLength, int patLength)
{
    textLength = txtLength;
    text = new T [textLength];                                  /// memoriafoglalas majd atmasolas
    for (int i = 0; i < textLength; i ++)
        text[i] = txt[i];

    patternLength = patLength;
    pattern = new T [patternLength];                            /// memoriafoglalas a minta es lps tomboknek majd a minta atmasolasa
    lps = new int [patternLength];
    for (int i = 0; i < patternLength; i ++)
        pattern[i] = pat[i];
}

/// Leiras: Destruktor, felszabaditja a tomboknek lefoglalt memoriat
/// Elofeltetelek: letezzen az adott objektum
/// Utofeltetelek: felszabadul a memoria
/// Parameterek: nincs
template <class T> KMP<T> :: ~KMP()
{
    delete [] text;                                             /// felszabaditasok
    delete [] pattern;
    delete [] lps;
}

/// Leiras: A minta(pattern) tomb elofeldolgozasa. Meghatarozzuk az lps tomb elemeit, ezaltal tudjuk majd a kereseskor kulonbozo elemek eseten hogy a minta(pattern)
        /// hanyadik poziciojatol kell ujrakezdeni a keresest. Tehat az lps tomb a minta(pattern) tomb minden karakterere tartalmazni fogja a leghosszabb valodi
        /// elotag hosszat amely egyben utotag is.
/// Elofeltetelek: A text es pattern tombok nem egyenloek, valamint pattern tomb hossza nem nulla.
/// Utofeltetelek: Meghatarozza az lps tomb ertekeit
/// Parameterek: nincs
template <class T> void KMP<T> :: LPSArray()
{
    int j = 0;                                                  /// leghosszabb elotag-utotag hossza
    int i = 1;

    lps[0] = 0;                                                 /// a nulladik elem hossza 0
    while (i < patternLength)                                   /// vegigmegyunk a mintan
    {
        if (pattern[i] == pattern[j])                           /// egyezes eseten mindket valtozoval tovabblepunk
        {
            j ++;
            lps[i] = j;                                         /// az i-edik elem erteke a novelt hossz lesz
            i ++;
        }
        else                                                    /// ha a ket elem kulonbozik
        {
            if (j != 0)                                         /// ha a leghosszabb elo-utotag hossza nem nulla, ez megkapja az eggyel kisebb elotagon levo lps elem erteket, nem lepunk tovabb
            {
                j = lps[j - 1];
            }
            else                                                /// ha nulla volt akkor az lps i-ik elemenek elo-utotag hossza is nulla lesz es tovabblepunk a kovetkezo elemre
            {
                lps[i] = 0;
                i ++;
            }
        }
    }
}

/// Leiras: Meghivja az LPSArray fuggvenyt, majd a  minta linearis keresese a szovegben, egyezes eseten visszateriti a minta
        /// elso elofordulasanak poziciojat.
/// Elofeltetelek: A text es pattern tombok hossza nem nulla es a ket tomb nem egyenlo
/// Utofeltetelek: Visszateriti a minta elso elofordulasanak poziciojat (-1 ha ez nem talalhato)
/// Parameterek: nincs
template <class T> int KMP<T> :: KMPSearch()
{
    int i = 0;                                                  /// text index
    int j = 0;                                                  /// pattern index
    LPSArray();                                                 /// az elofeldolgozas meghivasa

    while (i < textLength)                                      /// vegigmegyunk a text tombon
    {
        if (pattern[j] == text[i])                              /// egyetezes eseten mindket tombben tovabblepunk
        {
            j ++;
            i ++;
        }
        if (j == patternLength)                                 /// ha a minta indexe elerte a minta hosszat visszateritjuk a minta kezdopoziciojat a text-ben
        {
            return i - j;
        }
        else if (i < textLength && pattern[j] != text[i])       /// ha meg nem ertunk a szoveg vegere es a ket karakter kulobozik
        {
            if (j != 0)                                         /// ha nem a minta elejen vagyunk akkor visszalepunk a mintaban az lps tomb szerint, a szovegben maradunk azon a pozicion ahol elromlott az egyezes
                j = lps[j - 1];
            else                                                /// ha meg a minta elejen voltunk akkor csak a szovegben lepunk tovabb
                i ++;
        }
    }
    return -1;                                                  /// nincs benne a minta
}

/// Leiras: Meghivja az LPSArray fuggvenyt, majd a minta linearis keresese a szovegben, visszateriti a minta osszes elofordulasanak kezdopoziciojat
        /// a results vektorban.
/// Elofeltetelek: A text es pattern tombok hossza nem nulla es a ket tomb nem egyenlo
/// Utofeltetelek: Visszateriti a minta osszes elofordulasanak poziciojat a results vektorban
/// Parameterek: nincs
template <class T> vector<int> KMP<T> :: KMPSearchAll()       /// ugyanaz mint a KMPSearch alprogram, azzal a kulonbseggel hogy minden elofordulast visszaterit
{
    int i = 0;                                                  /// text index
    int j = 0;                                                  /// pattern index
    vector <int> results;                                       /// a result vektorban taroljuk az elofordulasok pozicioit
    LPSArray();                                                 /// az elofeldolgozas meghivasa

    while (i < textLength)
    {
        if (pattern[j] == text[i])                              /// egyetezes eseten mindket tombben tovabblepunk
        {
            j ++;
            i ++;
        }
        if (j == patternLength)                                 /// ha megtalaltuk a mintat, lementjuk a results vektorba es visszalepunk a minta valtozoval az lps tomb szerint
        {
            results.push_back(i - j);
            j = lps[j - 1];
        }
        else if (i < textLength && pattern[j] != text[i])       /// ha meg nem ertunk a szoveg vegere es a ket karakter kulobozik
        {
            if ( j != 0)                                        /// ha nem a minta elejen vagyunk akkor visszalepunk a mintaban az lps tomb szerint, a szovegben maradunk azon a pozicion ahol elromlott az egyezes
                j = lps[j - 1];
            else                                                /// ha meg a minta elejen voltunk akkor csak a szovegben lepunk tovabb
                i ++;
        }
    }
    return results;                                             /// visszateriti a talalt elofordulasokat
}

/// Leiras: Ellenorzi hogy a text es pattern tombok megegyeznek-e
/// Elofeltetelek: Letezik az adott objektum
/// Utofeltetelek: Igazat terit vissza egyenloseg eseten, kulonben hamisat
/// Parameterek: nincs
template <class T> bool KMP<T> :: Equal() const
{
    if (textLength != patternLength)                            /// kulobozo meret eseten hamis
        return 0;
    for (int i = 0; i < textLength; i ++)                       /// vegigmegyunk az elemeken, ha elteres van hamis a visszateritesi ertek
    {
        if (text[i] != pattern[i])
            return 0;
    }
    return 1;                                                   /// a ket karakterlanc egyforma
}

/// Leiras: Ellenorzi hogy a text tomb ures-e
/// Elofeltetelek: Letezik az adott objektum
/// Utofeltetelek: Igazat terit vissza ha a text hossza 0, kulonben hamisat
/// Parameterek: nincs
template <class T> bool KMP<T> :: TextEmpty() const
{
    return textLength == 0;
}

/// Leiras: Ellenorzi hogy a text tomb ures-e
/// Elofeltetelek: Letezik az adott objektum
/// Utofeltetelek: Igazat terit vissza ha a pattern hossza 0, kulonben hamisat
/// Parameterek: nincs
template <class T> bool KMP<T> :: PatternEmpty() const
{
    return patternLength == 0;
}

template class KMP<int>;
template class KMP<short int>;
template class KMP<long>;
template class KMP<long long>;
template class KMP<unsigned long>;
template class KMP<double>;
template class KMP<float>;
template class KMP<char>;
